def load_trajectories(path):
    import pandas as pd
    Trayectories = pd.read_csv(path, header=None)
    Encabezado = Trayectories[0][0] 
    Trayectories = Trayectories.rename(columns = {0:'history'}) 
    Trayectories = Trayectories.drop([0],axis=0) 	
    Encabezado = Encabezado.split(' ')
    print("Numero total de trayectorias calculadas:",Encabezado[0])
    print("Largo del video:",Encabezado[1])
    print("Ancho del video:",Encabezado[2])
    print("Numero total de frames del video:",Encabezado[3])
    return Trayectories, Encabezado

def filterByDistance(path):
    from tqdm import tqdm
    import pandas as pd
    import numpy as np
    Trayectories = pd.read_csv(path, header=None)
    Encabezado = Trayectories[0][0] 
    Trayectories = Trayectories.rename(columns = {0:'history'}) 
    Trayectories = Trayectories.drop([0],axis=0) 
    Encabezado = Encabezado.split(' ')
    TF = pd.DataFrame(columns=('history',))
    for i in tqdm(Trayectories['history']):
        t = i.split(' ')
        d = np.sqrt((float(t[2])-float(t[-3]))**2 + (float(t[3])-float(t[-2]))**2)
        if(d>5):
	        TF.loc[len(TF)]=[i,]
    Encabezado[0] = TF.shape[0]
    ##Retorna el encabezado antiguo ya q lo necesitamos en la funcion guardar cabecera
    print("Numero total de trayectorias calculadas:",Encabezado[0])
    print("Largo del video:",Encabezado[1])
    print("Ancho del video:",Encabezado[2])
    print("Numero total de frames del video:",Encabezado[3])
    print('------------------------------------------------')
    return TF,Encabezado

def filterByArea(path,x,y):
    import numpy as np
    import pandas as pd
    from tqdm import tqdm
    Trayectories = pd.read_csv(path,header=None)
    Encabezado = Trayectories[0][0] 
    Trayectories = Trayectories.rename(columns = {0:'history'}) 
    Trayectories = Trayectories.drop([0],axis=0)
    Encabezado = Encabezado.split(' ')
    TF = pd.DataFrame(columns=('history',))
    for i in tqdm(Trayectories['history']):
        t = i.split(' ')
        d = np.sqrt((float(t[2])-float(t[-3]))**2 + (float(t[3])-float(t[-2]))**2)
        x=2
        y=3 
        good = True
        for _ in range(16):
            if(d<4 or (float(t[y])<y[0] or float(t[y])>y[1] or float(t[x])<x[0] or float(t[x])>x[1])):
                good = False
                break
            x+=2
            y+=2
        if(good == True):
            TF.loc[len(TF)]=[i,]
    Encabezado[0] = TF.shape[0]
    print("Numero total de trayectorias calculadas:",Encabezado[0])
    print("Largo del video:",Encabezado[1])
    print("Ancho del video:",Encabezado[2])
    print("Numero total de frames del video:",Encabezado[3])
    print('------------------------------------------------')
    return TF,Encabezado


#Tienes que filtrar los datos antes de poder usar esta función
def Medium(path):
    import statistics as stats
    import pandas as pd
    Trayectories = pd.read_csv(path,header=None)
    Trayectories = Trayectories.rename(columns = {0:'history'})
    medianas = pd.DataFrame(columns=('history',))
    t1 = Trayectories['history'][0].split(' ')
    f1 = t1[0]
    f2 = t1[1]
    x=[] #lista que contendra las posiciones en x
    y=[] #lista que contendra las posiciones en y
    for i in Trayectories['history']:
        t = i.split(" ")
        px=2
        py=3
        if(f1 == t[0] and f2 == t[1]):
            for _ in range(16):
                x.append(float(t[px]))
                y.append(float(t[py]))
                px+=2
                py+=2
        else:
            medianax = stats.median(x)
            medianay = stats.median(y)
            x=[]
            y=[]
            medianas.loc[len(medianas)]=[f1+' '+f2+' '+str(medianax)+' '+str(medianay),]
            f1 = t[0]
            f2 = t[1]
            px=2
            py=3
            for _ in range(16):
                x.append(float(t[px]))
                y.append(float(t[py]))
                px+=2
                py+=2
    return medianas

#Esta funcion se aplica despues de aplicar los otros filtros
def filterByMedium(path, medianas, Encabezado):
    from tqdm import tqdm
    import pandas as pd
    Trayectories = pd.read_csv(path,header=None)
    Trayectories = Trayectories.rename(columns = {0:'history'}) #Cambio el nombre de la columna por uno mas intuitivo
    TF = pd.DataFrame(columns=('history',))
    index = 0
    median = medianas['history'][index].split(' ')
    for i in tqdm(Trayectories['history']):
        t = i.split(" ")
        if(t[0] == median[0] and t[1] == median[1]):
            px = (float(t[2])+float(t[4])+float(t[6])+float(t[8])+float(t[10])+float(t[12])+float(t[14])+float(t[16])+float(t[18])+float(t[20])+float(t[22])+float(t[24])+float(t[26])+float(t[28])+float(t[30])+float(t[32]))/16
            py = (float(t[3])+float(t[5])+float(t[7])+float(t[9])+float(t[11])+float(t[13])+float(t[15])+float(t[17])+float(t[19])+float(t[21])+float(t[23])+float(t[25])+float(t[27])+float(t[29])+float(t[31])+float(t[33]))/16
            d = np.sqrt((px-float(median[2]))**2 + (py-float(median[3]))**2)
            if(d<10):
                TF.loc[len(TF)]=[i,]
        else:
            if(index == (medianas.shape[0]-1)):
                break
            index+=1
            median = medianas['tray'][index].split(' ')
            px = (float(t[2])+float(t[4])+float(t[6])+float(t[8])+float(t[10])+float(t[12])+float(t[14])+float(t[16])+float(t[18])+float(t[20])+float(t[22])+float(t[24])+float(t[26])+float(t[28])+float(t[30])+float(t[32]))/16
            py = (float(t[3])+float(t[5])+float(t[7])+float(t[9])+float(t[11])+float(t[13])+float(t[15])+float(t[17])+float(t[19])+float(t[21])+float(t[23])+float(t[25])+float(t[27])+float(t[29])+float(t[31])+float(t[33]))/16
            d = np.sqrt((px-float(median[2]))**2 + (py-float(median[3]))**2)
            if(d<10):
                TF.loc[len(TF)]=[i,]
    Encabezado[0] = TF.shape[0]
    print("Numero total de trayectorias calculadas:",Encabezado[0])
    print("Largo del video:",Encabezado[1])
    print("Ancho del video:",Encabezado[2])
    print("Numero total de frames del video:",Encabezado[3])
    print('------------------------------------------------')
    return TF,Encabezado


def save_trajectories(TF,ruta_txt):
    TrayectoriasF = open(ruta_txt,'w')
    for i in tqdm(TF['history']):
        TrayectoriasF.write(i+"\n")
    TrayectoriasF.close()


def save_header(Encabezado,ruta):
    CabeceraF = open(ruta,'w')
    CabeceraF.write(str(Encabezado[0])+" "+Encabezado[1]+" "+Encabezado[2]+" "+Encabezado[3])
    CabeceraF.close()
