def positions(ruta):
    TrayectoriasF = pd.read_csv(ruta,header=None)
    TrayectoriasF = TrayectoriasF.rename(columns = {0:'history'})

    x=[] #lista que contendra las posiciones en x
    y=[] #lista que contendra las posiciones en y

    for t in TrayectoriasF['history']:
        t = t.split(' ')
        xe=[] #Lista temporal que contendra elementos temporales de x
        ye=[] #Lista temporal que contendra elementos temporales de y
        px=2
        py=3
        for _ in range(16):
            xe.append(float(t[px]))
            ye.append(float(t[py]))
            px+=2
            py+=2
        x.append(xe)
        y.append(ye)
    x = np.array(x)
    y = np.array(y)
    #x=x.reshape((x.shape[0]*x.shape[1]))
    #y=y.reshape((y.shape[0]*y.shape[1]))
    return x,y

def velocity(ruta):
    x, y = posiciones(ruta)
    vx=[]
    vy=[]
    for i in range(x.shape[0]):
        ex=[]
        ey=[]
        for k in range(15):
            ex.append((float(x[i][k+1])-float(x[i][k])))
            ey.append((float(y[i][k+1])-float(y[i][k])))
        vx.append(ex)
        vy.append(ey)
    vx = np.array(vx)
    vy = np.array(vy)
    #vx=vx.reshape((vx.shape[0]*vx.shape[1]))
    #vy=vy.reshape((vy.shape[0]*vy.shape[1]))
    return vx,vy

def acceleration(ruta):
    vx, vy = velocidades(ruta)
    ax=[]
    ay=[]
    for i in range(vx.shape[0]):
        ex=[]
        ey=[]
        for k in range(14):
            ex.append((vx[i][k+1])-vx[i][k])
            ey.append((vy[i][k+1])-vy[i][k])
        ax.append(ex)
        ay.append(ey)
    ax = np.array(ax)
    ay = np.array(ay)
    #ax=ax.reshape((ax.shape[0]*ax.shape[1]))
    #ay=ay.reshape((ay.shape[0]*ay.shape[1]))
    return ax,ay

def kimaticHistograms(path):
    print(path)
    px,py = posiciones(path)
    x=px.reshape((px.shape[0]*px.shape[1]))
    y=py.reshape((py.shape[0]*py.shape[1]))
    pos =  np.zeros(shape=(int(height),int(width)))
    for j in range(x.shape[0]):
        pos[int(x[j])][int(y[j])] += 1 

    vx,vy = velocidades(path)
    vel =  np.zeros(shape=(int(height),int(width)))
    #vel2 =  np.zeros(shape=(int(height),int(width)))
    for i in range(px.shape[0]):
        for k in range(px.shape[1]-1):
            valor = np.sqrt((vx[i][k])**2+(vy[i][k])**2)
            #vel[int(px[i][k])][int(py[i][k])] += valor
            if(vel[int(px[i][k])][int(py[i][k])]<valor):
                vel[int(px[i][k])][int(py[i][k])] = valor

    ax,ay = aceleraciones(path)
    acel =  np.zeros(shape=(int(height),int(width)))
    #acel2 =  np.zeros(shape=(int(height),int(width)))
    for i in range(px.shape[0]):
        for k in range(px.shape[1]-2):
            valor = np.sqrt((ax[i][k])**2+(ay[i][k])**2)
            #acel[int(px[i][k])][int(py[i][k])] +=valor
            if(acel[int(px[i][k])][int(py[i][k])]<valor):
                acel[int(px[i][k])][int(py[i][k])] = valor
    return pos,vel,acel


def plot_histogram(histogram):
    import matplotlib.pyplot as plt
    plt.figure(figsize=(20,8))
    plt.imshow(histogram)
    plt.colorbar()
