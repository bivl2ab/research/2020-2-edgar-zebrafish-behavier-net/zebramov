 
def Background(frames, thresh=10):	
    height, width, depth = frames[1].shape
    B = np.zeros(shape=(int(height),int(width),int(depth)))
    #computing background
    for i in range(len(frames)):
        B += frames[i]
    B =B/len(frames)
    B  = B.astype(np.uint8)
    bs = []
    for i in range(len(frames)):
        dst = cv.absdiff(frames[i],B )
        gray = cv.cvtColor(dst, cv.COLOR_BGR2GRAY)
        th, dst_th = cv.threshold(gray, thresh, 255, cv.THRESH_BINARY)
        bs.append(dst_th)
    return bs, B

def interative(bs):
	return interact(lambda index:plt.imshow(bs[index], cmap="gray"),
         index = widgets.IntSlider(min=0,max=(len(bs)-1),
                                   step=1,value=0))

def load_video(path):
	Video_1 = cv.VideoCapture(path)
	frames_V1 = [cv.cvtColor(Video_1.read()[1], cv.COLOR_BGR2RGB) 
		     for _ in range(2,int(Video_1.get(cv.CAP_PROP_FRAME_COUNT)))]
	return frames_V1


def CenterOfMass(imagen):
    height, width = imagen.shape
    num = 0
    contador = 0
    ingresar = True
    for i in range(width):#Esto es y
        for k in range(height):#Esto es X
            if(imagen[k][i]==255):
                num = num + i
                contador+=1
    if(contador != 0):
        px = num/(contador)
    else:
        px = 0 
    num = 0
    contador = 0
    for i in range(height):#Esto es y
        for k in range(width):#Esto es X
            if(imagen[i][k]==255):
                num = num + i
                contador+=1
    if(contador != 0):
        py = num/(contador)
    else:
        py = 0
    return px,py

def centroids(path):
    Video_1 = cv.VideoCapture(path)
    frames_V1 = [cv.cvtColor(Video_1.read()[1], cv.COLOR_BGR2RGB) 
                 for _ in range(2,int(Video_1.get(cv.CAP_PROP_FRAME_COUNT)))]
    bs,B =Background(frames_V1, thresh=33)
    cx = []
    cy = []
    for i in range(len(bs)):
        imagenbs = bs[i]
        px,py = CentroDeMasa_Edgar(imagenbs)
        #if(px == 0 or py == 0):
        #    cx.append(t1)
        #    cy.append(t2)
        #else:
        cx.append(px)
        cy.append(py)
        #t1 = px
        #t2 = py
    return cx,cy

def save_centroids(ruta,cx,cy):
    centroides = open(ruta,"w")
    centroides.write(str(cx[0])+" "+str(cy[0]))
    for i in range(len(cx)-1):
        centroides.write(" "+str(cx[i+1])+" "+str(cy[i+1]))
    centroides.close()

def load_centroids(ruta):
    centroides = open(ruta,"r")
    trayectoria = centroides.read()
    trayectoria = trayectoria.split(' ')
    type(trayectoria)
    i = 0 ; cx = []
    k = 1 ; cy = []
    for _ in range(int(len(trayectoria)/2)):
        cx.append(float(trayectoria[i]))
        cy.append(float(trayectoria[k]))
        i+=2
        k+=2
    return cx,cy

def paint_centerOfMass(index, frames, cx, cy):
	import maplotlib.pyplot as plt	
	plt.imshow(frames[index])
	plt.scatter(cx[index],cy[index])
	
